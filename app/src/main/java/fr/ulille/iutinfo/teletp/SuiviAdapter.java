package fr.ulille.iutinfo.teletp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SuiviAdapter /* TODO Q6.a */ extends RecyclerView.Adapter<SuiviAdapter.ViewHolder> {
    // TODO Q6.a
    private LayoutInflater lain;
    SuiviViewModel model;

    class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
        public TextView getQuestion(){ return itemView.findViewById(R.id.question);}
    }

    public SuiviAdapter(Context context, SuiviViewModel svm){
        lain = LayoutInflater.from(context);
        this.model = svm;
    }

    @NonNull
    @Override
    public SuiviAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = lain.inflate(R.layout.question_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SuiviAdapter.ViewHolder holder, int position) {
        String question = model.getQuestion(position);
        holder.getQuestion().setText(question);
    }

    @Override
    public int getItemCount() {
        return 0;
    }
    // TODO Q7
}
